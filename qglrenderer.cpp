/*
 * GStreamer
 * Copyright (C) 2009 Julien Isorce <julien.isorce@gmail.com>
 * Copyright (C) 2009 Andrey Nechypurenko <andreynech@gmail.com>
 * Copyright (C) 2010 Nuno Santos <nunosantos@imaginando.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <QGLWidget>
#include <QApplication>
#include <QDebug>
#include <QCloseEvent>
#include <QOpenGLContext>


#include <gst/video/video.h>
#include <gst/gl/gl.h>
#include <gst/gl/gstglfuncs.h>

#if IS_PI
#include <gst/gl/egl/gstegl.h>
#include <gst/gl/egl/gstgldisplay_egl.h>
#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <QtPlatformHeaders/QEGLNativeContext>
#else
#include <QX11Info>
#include <gst/gl/x11/gstgldisplay_x11.h>
#include <QtPlatformHeaders/QGLXNativeContext>
#endif

#include "gstthread.h"
#include "qglrenderer.h"
#include "pipeline.h"

#include <iostream>

QGLRenderer::QGLRenderer (QWidget * parent)
    :
QOpenGLWidget (parent),
gst_thread (NULL),
closing (false),
frame (NULL)
{
  move (20, 10);
  resize (640, 480);
}

QGLRenderer::~QGLRenderer ()
{
}

void
QGLRenderer::initializeGL ()
{

  native_context = this->context()->nativeHandle();

  GstGLContext *context;
  GstGLDisplay *display;

#if IS_PI
  display =
          (GstGLDisplay *) gst_gl_display_egl_new_with_egl_display(
              qvariant_cast<QEGLNativeContext>(native_context).display()
           );
#elif GST_GL_HAVE_PLATFORM_GLX
  display =
          (GstGLDisplay *) gst_gl_display_x11_new_with_display (
              // qvariant_cast<QGLXNativeContext>(native_context).display()
              QX11Info::display ()
          );
#endif

  /* FIXME: Allow the choice at runtime */
#if IS_PI
  context =
      gst_gl_context_new_wrapped (display,
      (guintptr) qvariant_cast<QEGLNativeContext>(native_context).context(),
      GST_GL_PLATFORM_EGL, GST_GL_API_GLES2);
#elif GST_GL_HAVE_PLATFORM_GLX
  context =
      gst_gl_context_new_wrapped (display,
      (guintptr) qvariant_cast<QGLXNativeContext>(native_context).context(),
      // (guintptr) glXGetCurrentContext (),
      GST_GL_PLATFORM_GLX, GST_GL_API_OPENGL);
#endif
  gst_object_unref (display);

  // We need to unset Qt context before initializing gst-gl plugin.
  // Otherwise the attempt to share gst-gl context with Qt will fail.
  this->doneCurrent ();
  this->gst_thread =
      new GstThread (display, context, SLOT (newFrame ()), this);
  this->makeCurrent ();

  QObject::connect (this->gst_thread, SIGNAL (finished ()),
      this, SLOT (close ()));
  QObject::connect (this, SIGNAL (closeRequested ()),
      this->gst_thread, SLOT (stop ()), Qt::QueuedConnection);

  initializeOpenGLFunctions();
  glClearColor(0,0,0,1);
  //glShadeModel(GL_FLAT);
  // glEnable(GL_DEPTH_TEST);
  // glEnable(GL_CULL_FACE);
  glEnable (GL_TEXTURE_2D);     // Enable Texture Mapping

  this->gst_thread->start ();
}


void
QGLRenderer::resizeGL (int width, int height)
{
  // Reset The Current Viewport And Perspective Transformation
  glViewport (0, 0, width, height);

}

void
QGLRenderer::newFrame ()
{
  Pipeline *pipeline = this->gst_thread->getPipeline ();
  if (!pipeline)
    return;
  if (this->frame)
    pipeline->queue_output_buf.put (this->frame);

  this->frame = pipeline->queue_input_buf.get ();
  this->paintGL();
}

static void
flushGstreamerGL (GstGLContext * context, void *data G_GNUC_UNUSED)
{
  context->gl_vtable->Flush ();
}

QImage
QGLRenderer::frameToImage() {
    QImage img;
    GstMemory *mem;
    GstVideoInfo v_info;
    GstVideoFrame v_frame;
    GstVideoMeta *v_meta;

    mem = gst_buffer_peek_memory (this->frame, 0);
    v_meta = gst_buffer_get_video_meta (this->frame);

    Q_ASSERT (gst_is_gl_memory (mem));

    GstGLMemory *gl_memory = (GstGLMemory *) mem;

    gst_gl_context_thread_add (gl_memory->mem.context, flushGstreamerGL, NULL);

    gst_video_info_set_format (&v_info, v_meta->format, v_meta->width,
        v_meta->height);

    gst_video_frame_map (&v_frame, &v_info, this->frame, GST_MAP_READ);

    std::cout << v_meta->format << std::endl;

    const QImage image(
                static_cast<const uchar *>(v_frame.data[0]),
                v_info.width,
                v_info.height,
                v_info.stride[0],
                QImage::Format_RGBA8888);

    img = image;
    img.detach();
    gst_video_frame_unmap(&v_frame);
    return img;
}

void
QGLRenderer::paintGL ()
{
    if (this->frame) {
#if IS_PI
        QImage img = frameToImage();
        QPixmap surface;
        surface.convertFromImage(img);
        QPainter p(this);
        p.setRenderHint(QPainter::SmoothPixmapTransform, 1);
        p.drawPixmap(QRect(0, 0, 640, 480), surface);
        p.end();
#else
      guint tex_id;
      GstMemory *mem;
      GstVideoInfo v_info;
      GstVideoFrame v_frame;
      GstVideoMeta *v_meta;

      mem = gst_buffer_peek_memory (this->frame, 0);
      v_meta = gst_buffer_get_video_meta (this->frame);

      Q_ASSERT (gst_is_gl_memory (mem));

      GstGLMemory *gl_memory = (GstGLMemory *) mem;

      gst_gl_context_thread_add (gl_memory->mem.context, flushGstreamerGL, NULL);

      gst_video_info_set_format (&v_info, v_meta->format, v_meta->width,
          v_meta->height);

      gst_video_frame_map (&v_frame, &v_info, this->frame,
          (GstMapFlags) (GST_MAP_READ | GST_MAP_GL));

      tex_id = *(guint *) v_frame.data[0];

      glEnable (GL_DEPTH_TEST);

      glEnable (GL_TEXTURE_2D);
      glBindTexture (GL_TEXTURE_2D, tex_id);
      if (glGetError () != GL_NO_ERROR) {
        qDebug ("failed to bind texture that comes from gst-gl");
        emit closeRequested ();
        return;
      }

      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

      glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glMatrixMode (GL_MODELVIEW);
      glLoadIdentity ();

      glBegin (GL_QUADS);
      // Back Face
      glTexCoord2f (0.0f, 0.0f);
      glVertex3f (-1.0f, -1.0f, -1.0f);
      glTexCoord2f (0.0f, 1.0f);
      glVertex3f (-1.0f, 1.0f, -1.0f);
      glTexCoord2f (1.0f, 1.0f);
      glVertex3f (1.0f, 1.0f, -1.0f);
      glTexCoord2f (1.0f, 0.0f);
      glVertex3f (1.0f, -1.0f, -1.0f);
      glEnd ();

      glLoadIdentity();
      glDisable(GL_DEPTH_TEST);
      glBindTexture (GL_TEXTURE_2D, 0);

      gst_video_frame_unmap (&v_frame);
#endif

    update();
  }
}

void
QGLRenderer::closeEvent (QCloseEvent * event)
{
  if (this->closing == false) {
    this->closing = true;
    emit closeRequested ();
    event->ignore ();
  }
}
